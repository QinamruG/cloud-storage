import { FastifyInstance } from "fastify";

export default function (api: FastifyInstance, opts: any, done: any) {
    api.get("/hi", async (request, reply) => {
        reply.send({ hello: "there" });
    });
    done();
}
