import { FastifyInstance } from "fastify";

export const setupRoutes = (server: FastifyInstance) => {
    server.register(require("./sample"), {
        prefix: "/sample",
    });
    server.register(require("./auth"), {
        prefix: "/auth",
    });
};
