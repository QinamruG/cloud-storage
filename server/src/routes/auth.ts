import { FastifyInstance } from "fastify";
import { User } from "../interfaces/user.interface";
import { isValidUserData } from "../middlewares/auth-validation";
import userModel from "../models/user.model";

export default function (api: FastifyInstance, opts: any, done: any) {
    api.post<{ Body: User; Response: User }>("/signup", async (request, reply) => {
        try {
            const { email, password } = request.body;

            if (!isValidUserData(email, password)) return reply.status(403);

            console.log(email, password);

            const existedUser = await userModel.findOne({ email: email, password: password });

            if (existedUser) return reply.status(403);

            const newUser: User = {
                email: email,
                password: password,
            };

            userModel.create(newUser);
            return await reply.status(200).send(JSON.stringify(newUser));
        } catch (e) {
            console.log(e);
            return reply.status(403);
        }
    });
    done();
}
