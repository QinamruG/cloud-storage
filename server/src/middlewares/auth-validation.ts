import validator from "validator";

export const isValidUserData = (email: string, password: string): boolean => {
    return validator.isEmail(email) && password.length > 6;
};
