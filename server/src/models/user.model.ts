import { model, Schema } from "mongoose";
import { User } from "../interfaces/user.interface";

const defaultDiskSpace = 1024 ** 3 * 10;

const schema = new Schema<User>({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    diskSpace: { type: Number, default: defaultDiskSpace },
    usedSpace: { type: Number, default: 0 },
    avatar: { type: String },
    // files: [{ type: Object, ref: "File" }],
});

export default model<User>("User", schema);
