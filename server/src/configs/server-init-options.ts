import { FastifyServerOptions } from "fastify";

export const getServerOptions = (): FastifyServerOptions => {
    if (process.env.NODE_ENV !== "production")
        return {
            logger: true,
        };
    return {};
};
