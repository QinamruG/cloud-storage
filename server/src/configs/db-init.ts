import { userModel } from "../models";
import mongoose from "mongoose";
import { FastifyInstance } from "fastify";
import fp from "fastify-plugin";

const models = { userModel };

type Options = {
    uri: string;
};

const initDbConnection = async (app: FastifyInstance, options: Options) => {
    try {
        mongoose.connection.on("connected", () => {
            app.log.info({ actor: "MongoDB" }, "connected");
        });
        mongoose.connection.on("disconnected", () => {
            app.log.error({ actor: "MongoDB" }, "disconnected");
        });

        app.decorate("db", { models });

        await mongoose.connect(options.uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });
    } catch (err) {
        console.error(err);
    }
};

export default fp(initDbConnection);
