export interface User {
    email: string;
    password: string;
    diskSpace?: number;
    usedSpace?: number;
    avatar?: String;
    // files: File[];
}
