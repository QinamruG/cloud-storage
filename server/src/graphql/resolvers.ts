import { users } from "..";

export const resolvers = {
    Query: {
        users: () => users,
    },
};
