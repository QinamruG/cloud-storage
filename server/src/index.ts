import fastify from "fastify";
import { getServerOptions } from "./configs/server-init-options";
import { setupRoutes } from "./routes";
import mongoose from "mongoose";
import { ApolloServer, gql } from "apollo-server-fastify";
import { userModel } from "./models";
import { User } from "./interfaces/user.interface";
import { resolvers } from "./graphql/resolvers";
import path from "path";
import fs from "fs";
import InitDb from "./configs/db-init";
require("dotenv").config({ path: "./.env" });

const PORT = Number(process.env.PORT) || 3000;
const DB_URI = process.env.MONGODB_URI || "";
const models = { userModel };
const typeDefs = fs.readFileSync(path.join(__dirname, "graphql/schema.graphql"), "utf8");

export const users = [
    new userModel({ email: "asdfjask@mail.ru", password: "1231rssdr3" }),
    new userModel({ email: "asdfjas@mail.ru", password: "1231rss" }),
    new userModel({ email: "asjask@mail.ru", password: "1231rssdr32" }),
];

const server = new ApolloServer({ typeDefs: typeDefs, resolvers: resolvers });

const app = fastify(getServerOptions());

app.register(require("fastify-express"));
app.register(require("fastify-cors"), {});

setupRoutes(app);

const start = async () => {
    InitDb(app, { uri: DB_URI });

    await server.start();

    app.register(server.createHandler());

    app.listen(PORT, (err, address) => {
        if (err) {
            app.log.error(err);
            process.exit(1);
        }
        app.log.info(`server listening on ${address}`);
        app.log.info(`gql on ${address}${server.graphqlPath}`);
    });
};

start();
